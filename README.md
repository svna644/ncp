# NCP

This is an example code on how to dynamically load a <video> element with Javascript. Really useful when, for example, you need to have a video featured on some resolutions only. Hiding the element with CSS display:none; will still make it load the video in the background which is inefficient.
  
This technique was used on [New Cape Picture's](https://newcapepictures.com/) website.
